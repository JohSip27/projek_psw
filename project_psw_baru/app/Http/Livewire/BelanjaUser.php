<?php

namespace App\Http\Livewire;
use Illuminate\Support\Facades\Auth;
use App\Models\belanja;
use Livewire\Component;

class BelanjaUser extends Component
{
    public $belanja = [];

    public function increaseQuantity($rowId)
    {
        $produk = belanja::get($rowId);
        $qty = $produk->qty + 1;
        belanja::update($rowId,$qty);
    }

    public function decreaseQuantity($rowId)
    {
        $produk = belanja::get($rowId);
        $qty = $produk->qty - 1;
        belanja::update($rowId,$qty);
    }

    public function destroy($pesanan_id)
    {
        $pesanan = belanja::find($pesanan_id);
        $pesanan->delete();
    }

    public function render()
    {
        if(Auth::user())
        {
            $this->belanja = belanja::where('user_id', Auth::user()->id)->get();
        }
        return view('livewire.belanja-user')
        ->extends('layouts.app')->section('content');

    }
}

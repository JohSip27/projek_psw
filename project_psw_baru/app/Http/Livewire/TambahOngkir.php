<?php

namespace App\Http\Livewire;

use Livewire\Component;
use kavist\RajaOngkir\RajaOngkir;

class TambahOngkir extends Component
{
    private $apiKey = '0a15a0792ae5a57c3ba076949db6bb8b';

    public function render()
    {
        $rajaOngkir = new RajaOngkir($this->apiKey);
        $daftarProvinsi = $rajaOngkir->provinsi()->all();
        dd($daftarProvinsi);
        return view('livewire.tambah-ongkir')->extends('layouts.app')->section('content');

    }
}

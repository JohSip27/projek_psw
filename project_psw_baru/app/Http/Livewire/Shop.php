<?php

namespace App\Http\Livewire;
use App\Models\produk;
use App\Models\belanja;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Livewire\Component;

class Shop extends Component
{
    public $produk = [];

    //filtering data
    public $search,$minimum,$maximum,$cari;

    public function beli($id)
    {
        if(!Auth::user())
        {
            return Redirect()->route('login');
        }

        $produk = produk::find($id);

        belanja::create(
            [
                'user_id' => Auth::user()->id,
                'total_harga' => $produk->harga,
                'produk_id' => $produk->id,
                'status' => 0
            ]
            );
        return redirect()->to('BelanjaUser');
    }
    public function render()
    {



        if($this->maximum)
        {
            $harga_max = $this->maximum;
        }
        else {
            $harga_max = 1000000000;
        }

        if($this->minimum)
        {
            $harga_min = $this->minimum;
        }
        else {
            $harga_min = 0;
        }

        if($this->search)
        {
            $this->produk = Produk::where('nama','like','%'.$this->search.'%')
                                         ->where('harga','>=',$harga_min)
                                         ->where('harga','<=',$harga_max)
                                         ->get();
        }
        else 
        {
            $this->produk = Produk::where('harga','>=',$harga_min)
                                    ->where('harga','<=',$harga_max)
                                    ->get();
        }

        if($this->cari)
        {
            $this->produk = Produk::where('nama','like','%'.$this->search.'%');
        }


        $this->produk = Produk::all();
        return view('livewire.shop')
        ->extends('layouts.app')->section('content');

    }
}

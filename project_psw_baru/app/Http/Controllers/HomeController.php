<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\produk;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }


    public function filter(Request $request)
    {
        if($request->has('search')) {
            $data = produk::where('nama','LIKE','%' .$request->search);
        } else {
            $data = produk::all();
        }
    }
}

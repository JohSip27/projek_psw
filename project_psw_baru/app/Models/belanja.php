<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class belanja extends Model
{
    use HasFactory;
    protected $table = 'belanja';
    protected $fillable = [
        'user_id',
        'produk_id',
        'total_harga',
        'status',
    ];
}

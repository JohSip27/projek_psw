<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/search', [HomeController::class , 'filter']);

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', App\Http\Livewire\Home::class);
Route::get('/TambahProduk', App\Http\Livewire\TambahProduk::class);
Route::get('/about', App\Http\Livewire\about::class);
Route::get('/contact', App\Http\Livewire\contact::class);
Route::get('/shop', App\Http\Livewire\shop::class);
Route::get('/BelanjaUser', App\Http\Livewire\BelanjaUser::class);
Route::get('/TambahOngkir', App\Http\Livewire\TambahOngkir::class);
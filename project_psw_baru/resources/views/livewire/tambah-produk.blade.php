<div class="container">
    <div class="card">
        <div class="card-header">
            <h5 class="text-center"><b>Tambah Produk</b></h5>

            
        </div>
        <div class="card-body">
            <form wire:submit.prevent="store">
                <div class="mb-3">
                  <label for="exampleInputEmail1" class="form-label">{{ ('Nama Produk') }}</label>
                  <input id="nama" type="text" class="form-control @error('nama') is-invalid @enderror" wire:model="nama" value="{{ old('nama') }}" required autocomplete="nama" autofocus>

                    @error('nama')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                </div>
                <div class="mb-3">
                  <label for="exampleInputPassword1" class="form-label">{{ ('Harga Produk') }}</label>
                  <input id="harga" type="number" class="form-control @error('harga') is-invalid @enderror" wire:model="harga" value="{{ old('harga') }}" required autocomplete="harga" autofocus>

                  @error('harga')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
                  @enderror

                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">{{ ('Berat Produk') }}</label>
                    <input id="berat" type="number" class="form-control @error('berat') is-invalid @enderror" wire:model="berat" value="{{ old('berat') }}" required autocomplete="berat" autofocus>

                    @error('berat')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                  </div>
                  <div class="mb-3">
                      <div id="emailHelp" class="form-text">{{ ('Gambar Produk (*maks 2 MB)') }}</div>
                    <input type="file" class="form-control" wire:model="gambar" id="" required>
                    @error('gambar')
                    <span class="error">{{ $message }}</span>
                    @enderror

                  </div>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
        </div>
    </div>
</div>